# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 09:52:03 2020

@author: test
"""

import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, InputLayer
import matplotlib.pyplot as plt
import funcs as f
import random
import time

from matplotlib import rc


font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 15}

rc('font', **font)

QL_inputs ={
    "T_max" : 2000.,
    "T_min" : 300.,
    
    "p_max" : 100e5,
    "p_min" : 1e5,
    
    "u_max" : 10,
    "u_min" : 0.01,
    
    "action": 1,
    
    "MAX_EPSILON" : 1,
    "MIN_EPSILON" : 0.01,
    
    "GAMMA" :       0.99,
    "BATCH_SIZE" :  50,
    
    
    
    "educts" :['CH4','O2','N2'],
    "products" :['H2','H2O','CH4'],
    "mech" : 'gri30.cti',
    
    "max_episodes": 500,
    "n_step": 200,
    "out": "Q7.txt",
    "start_time": time.clock()
    
    }




class Env:
    
    def __init__(self,QL_inputs):
        self.educts_strings = QL_inputs["educts"]
        self.products_strings = QL_inputs["products"]
        self.mech = QL_inputs["mech"]
        self.educts =f.normalize(np.ones(len(self.educts_strings)))
        self.T = (QL_inputs["T_max"]+QL_inputs["T_min"])/2
        self.p = (QL_inputs["p_max"]+QL_inputs["p_min"])/2
        self.u = (QL_inputs["u_max"]+QL_inputs["u_min"])/2
        # self.T = 1000
        # self.p = 10e5
        # self.u = 0.1
        
        
        self.products =np.zeros(len(self.products_strings))
        self.QL_inputs = QL_inputs
        
    def simple_pfr(self):
        import cantera as ct
        comp=""
        for i,species in enumerate(self.educts_strings):
            comp += species +":"+str(self.educts[i])+","
        comp = comp[:-1]
        length = 1.5  # *approximate* PFR length [m]
        time = length / self.u
        self.check_cons()
        gas1 = ct.Solution(self.mech)
        
        try:
            gas1.TPX = self.T, self.p, comp
        except: 
            print(self.T, self.p, comp)
            gas1.TPX = self.T, self.p, comp
        
        # gas1()
        # create a new reactor
        r1 = ct.IdealGasConstPressureReactor(gas1)
        # create a reactor network for performing time integration
        sim1 = ct.ReactorNet([r1])
        states1 = ct.SolutionArray(r1.thermo)
        try:
            while sim1.time < time:
                sim1.step()
                states1.append(r1.thermo.state)
            output=[]
            # gas1()
            
            for species in self.products_strings:
                try:
                    output.append(states1(species).X[-1][0])
                except:
                    e=Exception
                    output.append(0.)
                    print(e)
        except:
            e=Exception
            output =np.zeros(len(self.products_strings))
            print("error")
            print(e)
        self.products = output
        #print(output)
        return output
          
    def get_state(self):
        state = list(self.products)+list(self.educts)+[self.T/self.QL_inputs["T_max"],self.p/self.QL_inputs["p_max"],self.u/self.QL_inputs["u_max"]]
        #print(state)
        return np.array(state),self.products[0]*self.u
        
    def action(self,action,show):
        if action > len(self.educts)+2:
            change = 1.
            
        else:
            change =-1.
        
        decision = action%(len(self.educts)+3)
        
        if decision<len(self.educts):
            self.educts[decision] += change * self.QL_inputs["n_stepsize"]
            if show:
                print("Change "+self.educts_strings[decision]+" by factor "+str(change*self.QL_inputs["n_stepsize"]))
            
        else:
            i = decision-len(self.educts)
            if i==0:
                self.T += change * self.QL_inputs["T_step"]
                if show:
                    print("Change T by factor "+str(change*self.QL_inputs["T_step"] )+" to T="+str(self.T))
            elif i==1:
                self.p += change * self.QL_inputs["p_step"]
                if show:
                    print("Change p by factor "+str(change*self.QL_inputs["p_step"] )+" to p="+str(self.p))
            elif i==2:
                self.u += change * self.QL_inputs["u_step"]
                if show:
                    print("Change u by factor "+str(change*self.QL_inputs["u_step"] )+" to u="+str(self.u))
            else:
                print("error! i = "+str(i))
        
        self.educts = f.normalize(self.educts)
        
        
    def action2(self,action):
        for i in range(len(self.educts)):
            self.educts[i] += action[i]*self.QL_inputs["n_stepsize"]
        self.T += action[-1]*self.QL_inputs["T_step"]
        self.p += action[-2]*self.QL_inputs["p_step"]
        self.u += action[-3]*self.QL_inputs["u_step"]
        self.educts = f.normalize(self.educts)
            
        
        
        
       
    def check_cons(self):
        for i in range(len(self.educts)):
            if self.educts[i]<0.: self.educts[i]=0.0
            if self.educts[i]>1.: self.educts[i]=1.
        # Check if max/min -conditions are exceeded
        if self.p > self.QL_inputs["p_max"]: self.p = self.QL_inputs["p_max"]
        if self.p < self.QL_inputs["p_min"]: self.p = self.QL_inputs["p_min"]
        
        if self.T > self.QL_inputs["T_max"]: self.T = self.QL_inputs["T_max"]
        if self.T < self.QL_inputs["T_min"]: self.T = self.QL_inputs["T_min"]
        
        if self.u > self.QL_inputs["u_max"]: self.u = self.QL_inputs["u_max"]
        if self.u < self.QL_inputs["u_min"]: self.u = self.QL_inputs["u_min"]
            
    def reset(self):
        self.educts =f.normalize(np.ones(len(self.educts_strings)))
        self.T = (self.QL_inputs["T_max"]+self.QL_inputs["T_min"])/2
        self.p = (self.QL_inputs["p_max"]+self.QL_inputs["p_min"])/2
        self.u = (self.QL_inputs["u_max"]+self.QL_inputs["u_min"])/2
        self.products =np.zeros(len(self.products_strings))
        
        
    def fake_reward(self):
        T_goal = 1500
        p_goal = 27e5
        u_goal = 5
        x_methane_goal = 0.8
        x_ox_goal = 0.2
        x_n2_goal = 0.0
        reward = 0
        
        r1= -abs(self.T-T_goal)/self.QL_inputs["T_step"]
        r2= -abs(self.p-p_goal)/self.QL_inputs["p_step"]
        r3= -abs(self.u-u_goal)/self.QL_inputs["u_step"]
        r4= -abs(self.educts[0]-x_methane_goal)/self.QL_inputs["n_stepsize"]
        r5= -abs(self.educts[1]-x_ox_goal)/self.QL_inputs["n_stepsize"]
        r6= -abs(self.educts[2]-x_n2_goal)/self.QL_inputs["n_stepsize"]
        reward = sum([r1,r2,r3,r4,r5,r6])
        f = open("fake_reward.txt", "a")
        
        
        
        f.write("\t".join(str(e) for e in [r1,r2,r3,r4,r5,r6,reward])+"\n")
        f.close()
        #print(self.T,self.p,self.u,self.educts[0],self.educts[1],self.educts[2])
        #print(T_goal,p_goal,u_goal,x_methane_goal,x_ox_goal,x_n2_goal)
        #print("/n")
  
        return reward

def build_model(QL_inputs):
    model = Sequential()
    model.add(InputLayer(batch_input_shape=(None,QL_inputs["num_states"])))
    model.add(Dense(20,activation="relu"))
    model.add(Dense(20,activation="relu"))
    model.add(Dense(20,activation="relu"))
    if QL_inputs["action"]==1:
        model.add(Dense(QL_inputs["num_actions"],activation="linear"))
    elif QL_inputs["action"]==2:
       
        model.add(Dense(QL_inputs["num_actions"],activation="elu"))
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    
    return model

class Memory:
    def __init__(self, max_memory):
        self.max_memory = max_memory
        self.samples = []

    def add_sample(self, sample):
        self.samples.append(sample)
        if len(self.samples) > self.max_memory:
            self.samples.pop(0)

    def sample(self, no_samples):
        if no_samples > len(self.samples):
            return random.sample(self.samples, len(self.samples))
        else:
            return random.sample(self.samples, no_samples)
    
class Runner:
    def __init__(self,model,env,memory,QL_inputs):
        self.env      = env
        self.model    = model
        self.memory   = memory
        self.max_eps  = QL_inputs["MAX_EPSILON"]
        self.min_eps  = QL_inputs["MIN_EPSILON"]
        self.eps      = self.max_eps
        self.steps    = 0
        self.reward_s = []
        self.max_x_s  = []
        self.LAMBDA   = QL_inputs["LAMBDA"]
        self.bat_size = QL_inputs["BATCH_SIZE"]
        self.GAMMA    = QL_inputs["GAMMA"]
        self.num_actions = QL_inputs["num_actions"]
        self.num_states  = QL_inputs["num_states"]
        self.steps_total = 0 
        self.max_step    = QL_inputs["n_step"]
        self.action =QL_inputs["action"]
        self.out = QL_inputs["out"]
        self.start_time = QL_inputs["start_time"]
        self.n_steps_total = QL_inputs["max_episodes"]*self.max_step
        self.decay =0.1
        
    
        
        
    
    def run(self):
        self.env.reset()
        state,_ = self.env.get_state()
        reward = self.env.fake_reward() 
        self.steps = 0
        tot_reward = []
        max_x = 0
        done = False
        max_steps = self.max_step
        #self.plot_live(True)
        
        Ts = []
        ps = []
        us = []
        xs = []
        educts =[]
        for _ in self.env.educts:
            educts.append([])
            
        
        
        
        while True:
            action = self.choose_action(state)          # Choose action by model
            if self.action ==1:
                self.env.action(action,False)               # Execute action
            elif self.action ==2:
                self.env.action2(action)
            self.env.check_cons()                       # Check if max. or min. conditions are exceeded
            self.env.simple_pfr()                       # New-Cantera Calculation               
            next_state, reward = self.env.get_state()   # Get new state
            #reward = self.env.fake_reward()             # Get Fake reward to test algorithme on preset "optimal solution"
            
            
            Ts.append(self.env.T)
            ps.append(self.env.p)
            us.append(self.env.u)
            xs.append(self.env.products[0])
            for i in range(len(self.env.educts)):
                educts[i].append(self.env.educts[i])
                
            
            
            
            if next_state[0] > max_x:                   # Refresh max_x if it is exceeded in this step
                max_x = next_state[0]
            
            if self.steps > max_steps:                  # Check if max steps is reached
                done = True
            #     next_state = None
            
            #print("Step: "+str(self.steps))
            self.memory.add_sample((state,action,reward,next_state))    # Add current state,action,reward,next_state to internal memmory
            self.replay()                                               # 
            
            self.steps += 1    
            self.steps_total +=1                         # Increase Step-count and calc new epsilon
            self.eps = f.epsilon(self.steps_total,self.n_steps_total,self.decay)
            #self.eps = self.min_eps + (self.max_eps+self.min_eps)*np.exp(-self.LAMBDA * self.steps_total)
            
            state = next_state                          # move to next state
            tot_reward.append(reward)                        # add reward to total reward
            
            if done:
                mean = np.mean(tot_reward)
                self.reward_s.append(mean)
                self.max_x_s.append(max_x)
                
                return_data = Ts,ps,us,xs,educts
                s="Step {}, Mean reward: {}, Eps: {}".format(self.steps_total, mean, self.eps)
                file =open(self.out,"a")
                file.write("{} \t {} \t {} \t {} \n".format(self.steps_total, mean, self.eps, time.process_time()-self.start_time))
                file.close()
                print(s)
                return return_data
                
    def choose_action(self,state):
        if random.random() < self.eps:                                      # If RN is smaller then eps, choose a random action
            if self.action ==1:    
                return random.randint(0,self.model.output_shape[1]-1)
            elif self.action ==2:
                result = np.random.random(size=self.num_actions)
                for i in range(len(result)):
                    result[i] *=random.choice([-1,1])
                
                
                
                
                return result
        else:
            s = np.array([state])
            #print(s)
            result =self.model.predict(s)
            if self.action ==1:
                return np.argmax(result)                   # if not, choose action according to network
            elif self.action ==2:
                #print(result)
                return result[0]
        
        
    def replay(self):
        
        batch = self.memory.sample(self.bat_size)                     # Chooses Random set of stored Data

        states = np.array([[val[0] for val in batch]])
        next_states = np.array([[(np.zeros(self.num_states)if val[3] is None else val[3]) for val in batch]])
     
        q_s_a =[]

        for s in states:
            try:
                q_s_a.append(self.model.predict(s))                                                    # Predicts Q(s,a)
            except:
                #print(s)
                #print(np.shape(s))
                q_s_a.append(self.model.predict(s)) 
                   
        q_s_a_d =[]
        for s in next_states:
            q_s_a_d.append(self.model.predict(s))                             # Predicts Q(s',a') aka next state
  
        x = np.zeros((len(batch),self.model.input_shape[1]))                # Setup training arrays
        y = np.zeros((len(batch),self.model.output_shape[1]))
  
        # print("qsa")
        # print(q_s_a)
        # print("qsad")
        # print(q_s_a_d)
        for i,b in enumerate(batch):
            
            state,action,reward,next_state = b[0],b[1],b[2],b[3]
          
            current_q = q_s_a[0][i]
            # print("current q before")
            # print(current_q)
            # print("action")
            # print(action)
            if self.action==1:
                if next_state is None:
                    current_q[action]=reward
                else:
    
                    current_q[action]=reward + self.GAMMA * np.amax(q_s_a_d[0][i])
            elif self.action==2:
                if next_state is None:
                    for a in range(len(action)):
                        current_q[a]=reward
                else:
                    
                    for a in range(len(action)):
                        # print("np.amax")
                        # print(np.amax(q_s_a_d[0][i]))
                        current_q[a]=reward +self.GAMMA *np.amax(q_s_a_d[0][i])
                    
            # print("current q after")
            # print(current_q)  
            
            x[i] = state
            #print(y[i])
            
            y[i] = current_q
        # print("X")
        # print(x)
        # print("Y")
        # print(y)
        
        # time.sleep(5)
        self.model.train_on_batch(np.array(x),np.array(y))
        #print("dooooooone")
        

def plot_episode(episode,QL_inputs,cnt):
    eps = range(0,len(episode[0]))
    #n_eps=len(episode[0])
    
    for i in range(len(episode[1])):
        episode[1][i] /=100000
        episode[2][i] *=1000
     
    
    
    for i in range(len(episode[4])-1):
        
        
        for j in range(len(episode[4][i+1])):
            episode[4][i+1][j] += episode[4][i][j]
        #print(episode[4][i][0])
    
    
         
            
    
    
    #return_data = Ts,ps,us,xs,educts
    fig,axs = plt.subplots(5,figsize=(10,10))
    #fig.subplots_adjust(hspace=0.4,right=1.5)
    
    colors =["r","b","g","y"]
    
    dh = 0.9/len(episode[4])
             
    for i in range(len(episode[4])):
        h=dh*(i+1)
        
        axs[4].text(0.01, h, QL_inputs["educts"][i], transform=axs[4].transAxes, fontsize=14,
                    verticalalignment='top',color=colors[i])
    
    
    axs[0].plot(episode[3],"-k")                        # X_plot
    axs[1].plot(episode[0],"-k")                        # T plot
    axs[2].plot(episode[1],"-k")       # p plot
    axs[3].plot(episode[2],"-k")       # u plot
    
    axs[4].fill_between(eps,episode[4][0],color="r")
    for i in range(len(episode[4])-1):
        axs[4].fill_between(eps,episode[4][i],episode[4][i+1],color=colors[i+1])
        #axs[4].plot(episode[4][i],colors[i],label=QL_inputs["educts"][i])
        
    fig.suptitle("Episode "+str(cnt))
    
    axs[0].set_title("Mol-Fraction of "+QL_inputs["products"][0])
    axs[0].set_ylabel("X")
    
    axs[1].set_title("Temperature")
    axs[1].set_ylabel("T in K")
    axs[1].set_ylim(bottom=QL_inputs["T_min"]*0.8,top=QL_inputs["T_max"]*1.1)
    # axs[1].hlines(QL_inputs["T_min"],0,n_eps,alpha=0.5)
    # axs[1].hlines(QL_inputs["T_max"],0,n_eps,alpha=0.5)
    
    axs[2].set_title("Pressure")
    axs[2].set_ylabel("p in bar")
    axs[2].set_ylim(bottom=QL_inputs["p_min"]*0.8/100000,top=QL_inputs["p_max"]*1.1/100000)
    # axs[2].hlines(QL_inputs["p_min"],0,n_eps,alpha=0.5)
    # axs[2].hlines(QL_inputs["p_max"],0,n_eps,alpha=0.5)
    
    axs[3].set_title("Flow rate")
    axs[3].set_ylabel("u in mm/s")
    axs[3].set_ylim(bottom=QL_inputs["u_min"]*0.8*1000,top=QL_inputs["u_max"]*1.1*1000)
    # axs[3].hlines(QL_inputs["u_min"],0,n_eps,alpha=0.5)
    # axs[3].hlines(QL_inputs["u_max"],0,n_eps,alpha=0.5)
    
    
    axs[4].set_ylim(bottom=0,top=1)
    axs[4].set_ylabel("X")
    axs[4].set_title("Mol-Fractions of educts")
    
    
    fig.tight_layout()
    fig.suptitle("Episode: "+str(cnt))
    
    
    #fig.legend()
    if QL_inputs["action"]==1:
        plt.savefig("diagrams/"+str(cnt)+".svg")
    elif QL_inputs["action"]==2:
        plt.savefig("diagrams2/"+str(cnt)+".svg")
    
    plt.close()
    
def QL(QL_inputs):
    QL_inputs["num_states"] =  len(QL_inputs["products"])+len(QL_inputs["educts"])+3
    
    
    QL_inputs["T_step" ]   = (QL_inputs["T_max"]-QL_inputs["T_min"])/(QL_inputs["n_step"]*0.1)
    QL_inputs["p_step" ]   = (QL_inputs["p_max"]-QL_inputs["p_min"])/(QL_inputs["n_step"]*0.1)
    QL_inputs["u_step" ]   = (QL_inputs["u_max"]-QL_inputs["u_min"])/(QL_inputs["n_step"]*0.1)
    QL_inputs["n_stepsize"]= 0.1
    
    
    print(QL_inputs["num_states"])
    if QL_inputs["action"]==1:
        QL_inputs["num_actions"] = 2*(len(QL_inputs["educts"])+3)
    elif QL_inputs["action"]==2:
        QL_inputs["num_actions"] = len(QL_inputs["educts"])+3
    QL_inputs["LAMBDA"]=4./(QL_inputs["max_episodes"]*QL_inputs["n_step"])
    
    
    model   = build_model(QL_inputs)
    mem     = Memory(50000)
    env     = Env(QL_inputs)
    
    runner  = Runner(model,env,mem,QL_inputs)
    cnt     = 0
    
    while cnt < QL_inputs["max_episodes"]:
        
            
        return_data=runner.run()
        plot_episode(return_data,QL_inputs,cnt)
        #print("Episode {} of {} took {} seconds".format(cnt+1,QL_inputs["max_episodes"],time.process_time()-clock))
        #clock = time.process_time()
        cnt += 1
        
        
        
    plt.plot(runner.reward_s)
    plt.xlabel("Episodes")
    plt.ylabel("Total_reward")
    if QL_inputs["action"]==1:
        plt.savefig("diagrams/"+"reward"+".png")
    elif QL_inputs["action"]==2:
        plt.savefig("diagrams2/"+"reward"+".png")
    
    file = open("reward.txt","a")
    file.write("\n".join([str(x) for x in runner.reward_s]))
    file.close()
    
    plt.close()
    plt.xlabel("Episodes")
    plt.ylabel("Max. c(CH4)")
    plt.plot(runner.max_x_s)
    if QL_inputs["action"]==1:
        plt.savefig("diagrams/"+"max_x"+".png")
    elif QL_inputs["action"]==2:
        plt.savefig("diagrams2/"+"max_x"+".png")
    
    
def test_model(QL_inputs):
    QL_inputs["num_states"] =  len(QL_inputs["products"])+len(QL_inputs["educts"])+3
    if QL_inputs["action"]==1:
        QL_inputs["num_actions"] = 2*(len(QL_inputs["educts"])+3)
    elif QL_inputs["action"]==2:
        QL_inputs["num_actions"] = len(QL_inputs["educts"])+3
    QL_inputs["LAMBDA"]=10./(QL_inputs["max_episodes"]*QL_inputs["n_step"])
    
    
    model = build_model(QL_inputs)
    state = np.array([[0.06363265, 0.06363265, 0.1695412,  0.39423566, 0.43622314, 0.54250446, 0.65324828, 0.39173704]])
    print(state)
    print(model.predict(state))
    
    
if __name__ == "__main__":
    QL(QL_inputs)
    #test_model(QL_inputs)
    
        
        
    
                
        
        

            
            
            
            
            
            
        
        