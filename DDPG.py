# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 09:09:59 2020

@author: test
"""
import numpy as np

from tensorflow.keras import layers
import matplotlib.pyplot as plt
import funcs as f
# import random
import time
import tensorflow as tf
import os
from datetime import date
import pickle
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
tf.enable_eager_execution()

from matplotlib import rc


font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 15}

rc('font', **font)

DDPG ={
    "T_max" : 2000.,
    "T_min" : 300.,
    
    "p_max" : 100e5,
    "p_min" : 1e5,
    
    "u_max" : 10,
    "u_min" : 0.01,
    
    
    "BATCH_SIZE" :  128,
    
    "n_stepsize" : 0.1,
    
    
    "educts" :['CH4','O2','N2'],
    "products" :['H2','H2O','CH4'],
    "mech" : 'gri30.cti',
    "reactions": "all",
    
    "max_episodes": 500,
    "n_step": 200,
    
    "GAMMA": 0.9,
    "std": 0.02,
    "eps_max": 1.,
    "eps_min": 0.0001,
    "critic_lr": 0.002,
    "actor_lr": 0.001,
    "TAU": 0.005,
    "reward_function":"u*X",
    "memory": 5000,
    "model": "simple pfr",
    "epsilon_decay": 0.1 # [-1:1]
    
    
    }

def complete_DDPG(DDPG):
    DDPG["T_step"]=(DDPG["T_max"]-DDPG["T_min"])/(DDPG["n_step"]*0.1)
    DDPG["p_step"]=(DDPG["p_max"]-DDPG["p_min"])/(DDPG["n_step"]*0.1)
    DDPG["u_step"]=(DDPG["u_max"]-DDPG["u_min"])/(DDPG["n_step"]*0.1)
    DDPG["num_states"] =  len(DDPG["products"])+len(DDPG["educts"])+3
    DDPG["num_actions"] = len(DDPG["educts"])+3
    DDPG["LAMBDA"]=10./(DDPG["max_episodes"]*DDPG["n_step"])
    #print("Size of State Space ->  {}".format(DDPG["num_states"]))
    #print("Size of Action Space ->  {}".format(DDPG["num_actions"]))
    #current_path = os.path.abspath(os.getcwd())

    DDPG["date"]=date.today().strftime("%d_%m_%y")
    DDPG["start_time"]= time.process_time()
    
    return DDPG



class Env:
    
    def __init__(self,DDPG):
        self.educts_strings = DDPG["educts"]
        self.products_strings = DDPG["products"]
        self.mech = DDPG["mech"]
        self.educts =f.normalize(np.ones(len(self.educts_strings)))
        self.T = (DDPG["T_max"]+DDPG["T_min"])/2
        self.p = (DDPG["p_max"]+DDPG["p_min"])/2
        self.u = (DDPG["u_max"]+DDPG["u_min"])/2
        self.Xs = np.zeros(len(self.products_strings))
        self.Ys = np.zeros(len(self.products_strings))
        self.DDPG = DDPG
        self.out = DDPG["out"]
        self.massflow = 0.
        self.result = None
        self.carbon_deposite = 0.
        self.prev_result = None
        self.run_count = 0
    

        
    def simple_pfr(self):
        import cantera as ct
        #cm = 0.01
        #area = 1.0 * cm**2  # Catalyst bed area
        comp=""
        for i,species in enumerate(self.educts_strings):
            comp += species +":"+str(self.educts[i])+","
        
        comp +="C:"+str(self.carbon_deposite)
        length = 1.5  # *approximate* PFR length [m]
        time = length / self.u
        self.check_cons()
        gas1 = ct.Solution(self.mech)
        gas1.TPX = self.T, self.p, comp
        r1 = ct.IdealGasConstPressureReactor(gas1)
        # create a reactor network for performing time integration
        sim1 = ct.ReactorNet([r1])
        states1 = ct.SolutionArray(r1.thermo)
        
        #mass_flow_rate = self.u*0.6 * gas1.density * area
        #massflow_before = gas1.density
        
        try:
            while sim1.time < time:
                sim1.step()
                states1.append(r1.thermo.state)
            output = states1[-1]
           # massflow_after = gas1.density
            self.carbon_deposite = output("C").X[0]
            #print(self.carbon_deposite)
        except Exception as e:
            output = None
            #massflow_after = None
            print("Cantera Error")
            print(e)            
        #print("Massflow inlet: {} --> massflow outlet: {}".format(massflow_before,massflow_after))
        self.prev_result = self.result
        self.result = output
        
        return output
          
    def surface_pfr(self):
        import cantera as ct
        cm = 0.01
        
        length = 0.3 * cm  # Catalyst bed length
        area = 1.0 * cm**2  # Catalyst bed area
        cat_area_per_vol = 1000.0 / cm  # Catalyst particle surface area per unit volume
        #velocity = 40.0 * cm / minute  # gas velocity
        porosity = 0.3  # Catalyst bed porosity
        comp=""
        for i,species in enumerate(self.educts_strings):
            comp += species +":"+str(self.educts[i])+","
        comp = comp[:-1]
        NReactors = 201
        gas = ct.Solution(self.mech, 'gas')
        gas.TPX = self.T, self.p, comp
        surf = ct.Interface(self.mech,'Pt_surf', [gas])
        surf.TP = self.T, self.p
        rlen = length/(NReactors-1)
        rvol = area * rlen * porosity
        
        # catalyst area in one reactor
        cat_area = cat_area_per_vol * rvol
        
        mass_flow_rate = self.u*0.6 * gas.density * area
        self.massflow = mass_flow_rate*5e4
        #print("massflow: {}".format(self.massflow))
        # The plug flow reactor is represented by a linear chain of zero-dimensional
        # reactors. The gas at the inlet to the first one has the specified inlet
        # composition, and for all others the inlet composition is fixed at the
        # composition of the reactor immediately upstream. Since in a PFR model there
        # is no diffusion, the upstream reactors are not affected by any downstream
        # reactors, and therefore the problem may be solved by simply marching from
        # the first to last reactor, integrating each one to steady state.
        
        TDY = gas.TDY
        cov = surf.coverages
        
    
        
        # create a new reactor
        gas.TDY = TDY
        r = ct.IdealGasReactor(gas, energy='off')
        r.volume = rvol
        
        # create a reservoir to represent the reactor immediately upstream. Note
        # that the gas object is set already to the state of the upstream reactor
        upstream = ct.Reservoir(gas, name='upstream')
        
        # create a reservoir for the reactor to exhaust into. The composition of
        # this reservoir is irrelevant.
        downstream = ct.Reservoir(gas, name='downstream')
        
        # Add the reacting surface to the reactor. The area is set to the desired
        # catalyst area in the reactor.
        rsurf = ct.ReactorSurface(surf, r, A=cat_area)
        
        # The mass flow rate into the reactor will be fixed by using a
        # MassFlowController object.
        m = ct.MassFlowController(upstream, r, mdot=mass_flow_rate)
        
        # We need an outlet to the downstream reservoir. This will determine the
        # pressure in the reactor. The value of K will only affect the transient
        # pressure difference.
        v = ct.PressureController(r, downstream, master=m, K=1e-5)
        
        sim = ct.ReactorNet([r])
        sim.max_err_test_fails = 12
        
        # set relative and absolute tolerances on the simulation
        sim.rtol = 1.0e-9
        sim.atol = 1.0e-21
        states1 = ct.SolutionArray(r.thermo)
        try:
            for n in range(NReactors):
                # Set the state of the reservoir to match that of the previous reactor
                gas.TDY = r.thermo.TDY
                upstream.syncState()
                sim.reinitialize()
                sim.advance_to_steady_state()
                dist = n * rlen * 1.0e3   # distance in mm
                states1.append(r.thermo.state)
            
                
            
                
        
      
        
            output = states1[-1]
        except:
            output = None
        self.prev_result = self.result
        self.result = output
        return output
    
    
    def get_state(self):
        state = list(self.products)+list(self.educts)+[self.T/self.DDPG["T_max"],self.p/self.DDPG["p_max"],self.u/self.DDPG["u_max"]]
        time_yield = self.massflow * self.products[0]
        
        return np.array(state),time_yield
    
    def calc_reward(self,DDPG):
        state = np.array(list(self.Ys)+list(self.educts)+[self.T/self.DDPG["T_max"],self.p/self.DDPG["p_max"],self.u/self.DDPG["u_max"]])
        
        state.flatten()
        
        if True:
            if self.result is None:
                reward = 0.
            
            elif DDPG["reward_function"]=="u*X":
                reward = self.u * self.result(DDPG["products"][0]).X
            
            elif DDPG["reward_function"]=="massflow*Y":
                reward = self.massflow * self.result(DDPG["products"][0]).Y
                
            elif DDPG["reward_function"]=="massflow*X":
                reward = self.massflow * self.result(DDPG["products"][0]).X
                
            elif DDPG["reward_function"]=="X":
                reward =self.result(DDPG["products"][0]).X
               
            elif DDPG["reward_function"]=="Y":
                reward =self.result(DDPG["products"][0]).Y
               
            elif DDPG["reward_function"]=="gas_density*Y":
                reward =self.result(DDPG["products"][0]).Y*self.result.DP[0]
               
            elif DDPG["reward_function"]=="X+dX":
                if self.prev_result is None:
                    add = 0
                else:
                    add = self.result(DDPG["products"][0]).X - self.prev_result(DDPG["products"][0]).X
                reward = self.result(DDPG["products"][0]).X + add
                
               
            elif DDPG["reward_function"]=="dX":
                if self.prev_result is None:
                    reward = 0
                else:
                    dif = self.result(DDPG["products"][0]).X - self.prev_result(DDPG["products"][0]).X
                    if abs(dif) < 0.00001:
                        reward = 0.0
                    else:
                        reward = dif
                    
            if reward ==list:
                reward=reward[0]
        
        else:
            
            
            T_goal = 1500
            p_goal = 27e5
            u_goal = 2
            x_methane_goal = 0.8
            x_ox_goal = 0.2
            x_n2_goal = 0.0
            
            reward = 0
            r1= -abs(self.T-T_goal)/DDPG["T_step"]
            r2=  -abs(self.p-p_goal)/DDPG["p_step"]
            r3= -abs(self.u-u_goal)/DDPG["u_step"]
            r4= -abs(self.educts[0]-x_methane_goal)/DDPG["n_stepsize"]
            r5= -abs(self.educts[1]-x_ox_goal)/DDPG["n_stepsize"]
            r6= -abs(self.educts[2]-x_n2_goal)/DDPG["n_stepsize"]
            
            
            reward += sum([r1,r2,r3,r4,r5,r6])
            
            f = open("fake_reward_DDPG.txt", "a")
            
            # print(self.T,self.p,self.u,self.educts[0],self.educts[1],self.educts[2])
            # print(T_goal,p_goal,u_goal,x_methane_goal,x_ox_goal,x_n2_goal)
            # print(r1,r2,r3,r4,r5,r6)
            # print(reward)
            # print("/n")
            
            f.write("\t".join(str(e) for e in [r1,r2,r3,r4,r5,r6,reward])+"\n")
            f.close()
            
            
            if self.result is not None:
                Xs=[]  
                Ys=[]
                for species in DDPG["products"]:
                    Xs.append(float(self.result(species).X))
                    Ys.append(float(self.result(species).Y))
            # print("Xs:")
            # print(Xs)
            # print("Ys:")
            # print(Ys)
                self.Xs = Xs
                self.Ys = Ys
                
            
        #print(reward)
        return np.array(state),reward
        
    def action(self,action):
        
        for i in range(len(self.educts)):
            if abs(action[0][i])>0.2:
                self.educts[i] += action[0][i]*self.DDPG["n_stepsize"]
                
        if abs(action[0][-1])>0.2:
            self.T += action[0][-1]*self.DDPG["T_step"]
            
        if abs(action[0][-2])>0.2:
            self.p += action[0][-2]*self.DDPG["p_step"]
            
        if abs(action[0][-3])>0.2:
            self.u += action[0][-3]*self.DDPG["u_step"]
        self.educts = f.normalize(self.educts)
        
        
    def step_action(self,action):
        #print(action)
        #Educt controll
        for i in range(len(self.educts)):
            if action[0][i]>0.5:
                self.educts[i] += self.DDPG["n_stepsize"]
            elif action[0][i]<-0.5:
                self.educts[i] -= self.DDPG["n_stepsize"]
        self.educts = f.normalize(self.educts)
        #Conditions Controll
        
        if action[0][-1]>0.5:
            self.T += self.DDPG["T_step"]
        elif action[0][-1]<-0.5:
            self.T -= self.DDPG["T_step"]
            
        if action[0][-2]>0.5:
            self.p += self.DDPG["p_step"]
        elif action[0][-2]<-0.5:
            self.p -= self.DDPG["p_step"]
            
        if action[0][-3]>0.5:
            self.u += self.DDPG["u_step"]
        elif action[0][-3]<-0.5:
            self.u -= self.DDPG["u_step"]
            
            
            
    def run_model(self):
        self.check_cons()
        if self.DDPG["model"]=="simple pfr":
            self.simple_pfr()
        elif self.DDPG["model"]=="surface pfr":
            self.surface_pfr()
        elif self.DDPG["model"]=="cstr":
            self.constV()
       
    def check_cons(self):
        for i in range(len(self.educts)):
            if self.educts[i]<0.: self.educts[i]=0.0
            if self.educts[i]>1.: self.educts[i]=1.
        # Check if max/min -conditions are exceeded
        if self.p > self.DDPG["p_max"]: self.p = self.DDPG["p_max"]
        if self.p < self.DDPG["p_min"]: self.p = self.DDPG["p_min"]
        
        if self.T > self.DDPG["T_max"]: self.T = self.DDPG["T_max"]
        if self.T < self.DDPG["T_min"]: self.T = self.DDPG["T_min"]
        
        if self.u > self.DDPG["u_max"]: self.u = self.DDPG["u_max"]
        if self.u < self.DDPG["u_min"]: self.u = self.DDPG["u_min"]
            
    def reset(self):
        self.educts =f.normalize(np.ones(len(self.educts_strings)))
        self.T = (self.DDPG["T_max"]+self.DDPG["T_min"])/2
        self.p = (self.DDPG["p_max"]+self.DDPG["p_min"])/2
        self.u = (self.DDPG["u_max"]+self.DDPG["u_min"])/2
        self.products =np.zeros(len(self.products_strings))
        self.carbon_deposite = 0


            
class Buffer:
    def __init__(self,DDPG):
        # Number of "experiences" to store at max
        self.buffer_capacity = DDPG["memory"]
        # Num of tuples to train on.
        self.batch_size = DDPG["BATCH_SIZE"]

        # Its tells us num of times record() was called.
        self.buffer_counter = 0

        # Instead of list of tuples as the exp.replay concept go
        # We use different np.arrays for each tuple element
        self.state_buffer = np.zeros((self.buffer_capacity, DDPG["num_states"]))
        self.action_buffer = np.zeros((self.buffer_capacity, DDPG["num_actions"]))
        self.reward_buffer = np.zeros((self.buffer_capacity, 1))
        self.next_state_buffer = np.zeros((self.buffer_capacity, DDPG["num_states"]))

    # Takes (s,a,r,s') obervation tuple as input
    def record(self, obs_tuple):
        # print(obs_tuple)
        # print(obs_tuple[0])
        # Set index to zero if buffer_capacity is exceeded,
        # replacing old records
        if self.buffer_counter<self.buffer_capacity:
            index = self.buffer_counter
        else:
            index = np.random.randint(0,self.buffer_capacity)
        #index = self.buffer_counter % self.buffer_capacity

        self.state_buffer[index] = obs_tuple[0]
        self.action_buffer[index] = obs_tuple[1][0]
        self.reward_buffer[index] = obs_tuple[2]
        self.next_state_buffer[index] = obs_tuple[3]

        self.buffer_counter += 1

    


        
def get_actor(DDPG):
    # Initialize weights between -3e-3 and 3-e3
    last_init = tf.random_uniform_initializer(minval=-0.003, maxval=0.003)

    inputs = layers.Input(shape=(DDPG["num_states"],))
    out = layers.Dense(64, activation="relu")(inputs)
    out = layers.Dense(64, activation="relu")(out)
    outputs = layers.Dense(DDPG["num_actions"], activation="tanh", kernel_initializer=last_init)(out)

    # Our upper bound is 2.0 for Pendulum.
    outputs = outputs * 1.0
    model = tf.keras.Model(inputs, outputs)
    return model


def get_critic(DDPG):
    # State as input
    state_input = layers.Input(shape=(DDPG["num_states"]))
    state_out = layers.Dense(16, activation="relu")(state_input)
    state_out = layers.Dense(32, activation="relu")(state_out)

    # Action as input
    action_input = layers.Input(shape=(DDPG["num_actions"]))
    action_out = layers.Dense(32, activation="relu")(action_input)

    # Both are passed through seperate layer before concatenating
    concat = layers.Concatenate()([state_out, action_out])

    out = layers.Dense(64, activation="relu")(concat)
    out = layers.Dense(64, activation="relu")(out)
    outputs = layers.Dense(1)(out)

    # Outputs single value for give state-action
    model = tf.keras.Model([state_input, action_input], outputs)

    return model

def policy(state,eps,DDPG):
    
    sampled_actions = tf.squeeze(DDPG["networks"].actor_model(state))
    
    if eps<np.random.random():
        sampled_actions = sampled_actions.numpy()
        legal_action = np.clip(sampled_actions, -1.0, 1.0)
    else:
        legal_action = []
        for i in range(DDPG["num_actions"]):
            legal_action.append(np.random.random()*np.random.choice([-1,1]))
    # #print(sampled_actions)
    # noise = noise_object()
    # # Adding noise to action
    # sampled_actions = sampled_actions.numpy() + noise

    # # We make sure action is within bounds
    # legal_action = np.clip(sampled_actions, -1.0, 1.0)
    # #print(legal_action)
    return [np.squeeze(legal_action)],DDPG


def print_DDPG(DDPG):
    file = open(DDPG["out"],"w")
    string="START OF INPUT DATA \n"
    file.write(string)
    for i in DDPG:
        string = str(i)
        for j in range(20-len(string)):
            string += " "
        
        string +=":"+"\t" +str(DDPG[i]) + "\n"
        file.write(string)
    string = "END OF INPUT DATA \n"
    file.write(string)
    file.close()
        
        
def plot_episode(episode,QL_inputs,cnt):
   
    Ts,ps,us,rs,xs,ys,educts = episode
    
    ## Save Gif
    save = QL_inputs["diagramms"] + "/"
    data = [None,None,None,None]
    data[0]=list(Ts)
    data[1]=list(ps)
    data[2]=list(us)
    data[3]=list(xs)
    data = np.array([data])
    max_cons = [int(QL_inputs["T_max"]),int(QL_inputs["p_max"]),int(QL_inputs["u_max"])]
    
    
    
    eps = range(0,len(episode[0]))
    #n_eps=len(episode[0])
    
    for i in range(len(ps)):
        ps[i] /=100000
        us[i] *=1000
     
    
    
    for i in range(len(educts)-1):
        
        
        for j in range(len(educts[i+1])):
            educts[i+1][j] += educts[i][j]
        #print(episode[4][i][0])
    
    ob = {"data":data,
          "cnt": cnt,
          "max_cons": max_cons,
          "save": save,
          "educts": educts
          }
    with open(save+"gif_data_"+str(cnt),"wb") as config_dictionary_file:
        pickle.dump(ob,config_dictionary_file)
    
    
    
    
    ## End Save Gif
    
         
            
    
    
    #return_data = Ts,ps,us,xs,educts
    fig,axs = plt.subplots(6,figsize=(10,12))
    #fig.subplots_adjust(hspace=0.4,right=1.5)
    
    colors =["r","b","g","y","k"]
    
    dh = 0.9/len(educts)
             
    for i in range(len(educts)):
        h=dh*(i+1)
        
        axs[4].text(0.01, h, QL_inputs["educts"][i], transform=axs[4].transAxes, fontsize=14,
                    verticalalignment='top',color=colors[i])
    
    
    axs[0].plot(rs,"-k")                        # Reward
    axs[1].plot(Ts,"-k")                        # T plot
    axs[2].plot(ps,"-k")       # p plot
    axs[3].plot(us,"-k")       # u plot
    
    axs[5].plot(xs,"-k")
    axs[5].plot(ys,"-r")
    
    axs[4].fill_between(eps,educts[0],color="r")
    for i in range(len(educts)-1):
        axs[4].fill_between(eps,educts[i],educts[i+1],color=colors[i+1])
        #axs[4].plot(episode[4][i],colors[i],label=QL_inputs["educts"][i])
        
    fig.suptitle("Episode "+str(cnt))
    
    axs[0].set_title("Reward")
    axs[0].set_ylabel("Reward")
    
    axs[1].set_title("Temperature")
    axs[1].set_ylabel("T in K")
    axs[1].set_ylim(bottom=QL_inputs["T_min"]*0.8,top=QL_inputs["T_max"]*1.1)
    # axs[1].hlines(QL_inputs["T_min"],0,n_eps,alpha=0.5)
    # axs[1].hlines(QL_inputs["T_max"],0,n_eps,alpha=0.5)
    
    axs[2].set_title("Pressure")
    axs[2].set_ylabel("p in bar")
    axs[2].set_ylim(bottom=QL_inputs["p_min"]*0.8/100000,top=QL_inputs["p_max"]*1.1/100000)
    # axs[2].hlines(QL_inputs["p_min"],0,n_eps,alpha=0.5)
    # axs[2].hlines(QL_inputs["p_max"],0,n_eps,alpha=0.5)
    
    axs[3].set_title("Flow rate")
    axs[3].set_ylabel("u in mm/s")
    axs[3].set_ylim(bottom=QL_inputs["u_min"]*0.8*1000,top=QL_inputs["u_max"]*1.1*1000)
    # axs[3].hlines(QL_inputs["u_min"],0,n_eps,alpha=0.5)
    # axs[3].hlines(QL_inputs["u_max"],0,n_eps,alpha=0.5)
    
    
    axs[4].set_ylim(bottom=0,top=1)
    axs[4].set_ylabel("X")
    axs[4].set_title("Mol-Fractions of educts")
    
    axs[5].set_ylim(bottom=0,top=1)
    axs[5].set_ylabel("X or Y")
    axs[5].set_title("X("+QL_inputs["products"][0]+") in black"+"\t"+"Y("+QL_inputs["products"][0]+") in red")
    
    fig.tight_layout()
    fig.suptitle("Episode: "+str(cnt))
    
    
    #fig.legend()
    
    plt.savefig(QL_inputs["diagramms"]+"/"+str(cnt)+".svg")
    
    plt.close()


class Networks:
    def __init__(self,DDPG):
        self.actor_model = get_actor(DDPG)
        self.critic_model = get_critic(DDPG)
        self.target_actor = get_actor(DDPG)
        self.target_critic = get_critic(DDPG)
        self.target_actor.set_weights(self.actor_model.get_weights())
        self.target_critic.set_weights(self.critic_model.get_weights())
        self.critic_optimizer =tf.keras.optimizers.Adam(DDPG["critic_lr"])
        self.actor_optimizer =tf.keras.optimizers.Adam(DDPG["actor_lr"])
     
    @tf.function
    def update_target(self,DDPG,string):
        if string == "actor":
            for (a, b) in zip(self.target_actor.variables, self.actor_model.variables):
                a.assign(b * DDPG["TAU"] + a * (1 - DDPG["TAU"]))
        if string == "critic":
            for (a, b) in zip(self.target_critic.variables, self.critic_model.variables):
                a.assign(b * DDPG["TAU"] + a * (1 - DDPG["TAU"]))
    
    
    # Eager execution is turned on by default in TensorFlow 2. Decorating with tf.function allows
    # ensorFlow to build a static graph out of the logic and computations in our function.
    # ThTis provides a large speed up for blocks of code that contain many small TensorFlow operations such as this one.
    @tf.function
    def update(self, state_batch, action_batch, reward_batch, next_state_batch,DDPG,):
        # Training and updating Actor & Critic networks.
        # See Pseudo Code.
        
        #target_critic = self.target_critic
        #actor_model = self.actor_model
        #critic_model = self.actor_model
        #critic_optimizer = self.critic_optimizer
        #actor_optimizer = self.actor_optimizer
        
        # Training and updating Actor & Critic networks.
        # See Pseudo Code.
        
        with tf.GradientTape() as tape:
            target_actions = self.target_actor(next_state_batch, training=True)
            y = reward_batch + DDPG["GAMMA"] * self.target_critic(
                [next_state_batch, target_actions], training=True
            )
            critic_value = self.critic_model([state_batch, action_batch], training=True)
            critic_loss = tf.math.reduce_mean(tf.math.square(y - critic_value))
        
        critic_grad = tape.gradient(critic_loss, self.critic_model.trainable_variables)
        
        
        self.critic_optimizer.apply_gradients(
            zip(critic_grad, self.critic_model.trainable_variables)
        )
        
        with tf.GradientTape() as tape:
            actions = self.actor_model(state_batch, training=True)
            critic_value = self.critic_model([state_batch, actions], training=True)
            # Used `-value` as we want to maximize the value given
            # by the critic for our actions
            actor_loss = -tf.math.reduce_mean(critic_value)
            
        
        actor_grad = tape.gradient(actor_loss, self.actor_model.trainable_variables)
        self.actor_optimizer.apply_gradients(
            zip(actor_grad, self.actor_model.trainable_variables)
        )
        
        
       

    # We compute the loss and update parameters
    def learn(self,DDPG):
        
        # Get sampling range
        record_range = min(DDPG["buffer"].buffer_counter, DDPG["buffer"].buffer_capacity)
        # Randomly sample indices
        batch_indices = np.random.choice(record_range, DDPG["buffer"].batch_size)

        # Convert to tensors
        state_batch = tf.convert_to_tensor(DDPG["buffer"].state_buffer[batch_indices])
        action_batch = tf.convert_to_tensor(DDPG["buffer"].action_buffer[batch_indices])
        reward_batch = tf.convert_to_tensor(DDPG["buffer"].reward_buffer[batch_indices])
        reward_batch = tf.cast(reward_batch, dtype=tf.float32)
        next_state_batch = tf.convert_to_tensor(DDPG["buffer"].next_state_buffer[batch_indices])

        self.update(state_batch, action_batch, reward_batch, next_state_batch,DDPG)
        
# This update target parameters slowly
# Based on rate `tau`, which is much less than one.
    
              
        
def run(DDPG):
    import numpy as np
    # from tensorflow.keras.models import Sequential
    # from tensorflow.keras.layers import Dense, InputLayer
    from tensorflow.keras import layers
    import matplotlib.pyplot as plt
    import funcs as f
    # import random
    import time
    import tensorflow as tf
    import os
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    tf.enable_eager_execution()
    #std_dev = 0.2
    DDPG = complete_DDPG(DDPG)
    
    DDPG["networks"]=Networks(DDPG)
    DDPG["buffer"] = Buffer(DDPG)
    max_reward = 0
    max_state = None
    
    # To store average reward history of last few episodes
    avg_reward_list = []
    
    eps_max = 1.
    total_steps_max = DDPG["max_episodes"]*DDPG["n_step"]
    eps_min = 0.001
    lam = 0.1*50/total_steps_max
    total_steps=0
    if not os.path.exists(DDPG["date"]):
        os.makedirs(DDPG["date"])
    
    subdirectories= [x[0] for x in os.walk(DDPG["date"])]
    DDPG["diagramms"]=DDPG["date"] +"/" +str(len(subdirectories))
    
    if not os.path.exists(DDPG["diagramms"]):
        os.makedirs(DDPG["diagramms"])
        
    DDPG["out"]=DDPG["diagramms"]+"/"+"output_"+DDPG["date"]+"_run_"+str(len(subdirectories))+".txt"
    env = Env(DDPG)
    # Takes about 4 min to train
    print_DDPG(DDPG)
    for ep in range(DDPG["max_episodes"]):
        env.reset()
        prev_state,_ = env.get_state()
        
        
        
        Ts = []
        ps = []
        us = []
        xs = []
        educts =[]
        rs = []
        ys =[]
        eps=1
        for _ in env.educts:
            educts.append([])
        
        
        for cnt in range(DDPG["n_step"]):
            if cnt%100 ==0:
                print("Episode: {}, Step: {}, Epsilon: {}".format(ep,cnt,eps))
            total_steps += 1
            eps = f.epsilon(total_steps,total_steps_max,DDPG["epsilon_decay"])
            tf_prev_state = tf.expand_dims(tf.convert_to_tensor(prev_state), 0)
    
            action,DDPG = policy(tf_prev_state,eps,DDPG)
            env.action(action)
            #env.step_action(action)
            env.check_cons()
            env.run_model()
           
            state, reward= env.calc_reward(DDPG)
            if reward > max_reward:
                print("New max. reward: {}".format(reward))
                max_reward = reward
                #print(state)
                max_state = state
                
            DDPG["buffer"].record((prev_state, action, reward, state))
            
    
            DDPG["networks"].learn(DDPG)
            
            DDPG["networks"].update_target(DDPG,"actor")
            DDPG["networks"].update_target(DDPG,"critic")
    
            
            Ts.append(env.T)
            ps.append(env.p)
            us.append(env.u)
            rs.append(reward)
            for i in range(len(env.educts)):
                    educts[i].append(env.educts[i])
            if env.result is not None:
                xs.append(env.Xs[0])
                ys.append(env.Ys[0])
                
            else:
                xs.append(0)
                ys.append(0)
        
            # End this episode when `done` is True
            
    
            prev_state = state
            #printProgressBar(i + 1, DDPG["n_step"], prefix = 'Progress:', suffix = 'Complete', length = 50)
    
        
        plot_data =Ts,ps,us,rs,xs,ys,educts
        plot_episode(plot_data,DDPG,ep)
        cnt +=1
    
        
        avg_reward = np.mean(rs)
        file = open(DDPG["out"],"a")
        file.write("{} \t {} \t {} \t {} \n".format(total_steps, avg_reward,eps,time.process_time()-DDPG["start_time"]))
        file.close()
        print("Episode * {} * Avg Reward is ==> {}, EPSILON = {}".format(ep, avg_reward,eps))
        avg_reward_list.append(avg_reward)
    
    # Plotting graph
    # Episodes versus Avg. Rewards
    
    plt.plot(avg_reward_list)
    file = open(DDPG["out"][:-3]+"_reward.txt","a")
    file.write("\n".join([str(x) for x in avg_reward_list]))
    file.close()
    plt.xlabel("Episode")
    plt.ylabel("Avg. Epsiodic Reward")
    plt.savefig(DDPG["out"][:-3]+"png")
    plt.close()
    print("Best Reward of {} was achived with state {}".format(max_reward,max_state))
    return None
    
if __name__ == "__main__":
    run(DDPG)