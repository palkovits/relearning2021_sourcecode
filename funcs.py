# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 22:01:51 2020

@author: test
"""
import numpy as np
import time

def timer(x,t):
    x += time.process_time()-t
    t = time.process_time()
    return x,t


def normalize(liste):
    s=sum(liste)
    out=[]
    for element in liste:
        out.append(float(element/s))
    return out

def arrhenius(A,n,Ea,T):
    R=1.987
    return A*(T**n)*np.exp(-Ea/(R*T))


def add_strings(strings):
    out =[]
    for s in strings:
        if not s=="":
            out.append(s)
    return out

def epsilon(step,max_step,s):
    return 1-(step/max_step)**(1/(10**s))

def epsilon2(step,max_step,s):
    decline = int(max_step/4.4)
    stay = int(max_step*0.2/4.4)
    if step<=decline:
        return epsilon(step,decline,s)
    elif step<=decline+stay:
        return 0.0
    elif step<=2*decline+stay:
        return epsilon(step-decline-stay,decline,s)
    elif step <=2*decline + 2*stay:
        return 0.0
    elif step<=3*decline+2*stay:
        return epsilon(step-2*decline-stay,decline,s)
    else:
        return 0.0
    
    

def print_options(DDPG):
    file = open(DDPG["out"],"w")
    string="START OF INPUT DATA \n"
    file.write(string)
    for i in DDPG:
        string = str(i)
        for j in range(20-len(string)):
            string += " "
        
        string +=":"+"\t" +str(DDPG[i]) + "\n"
        file.write(string)
    string = "END OF INPUT DATA \n"
    file.write(string)
    file.close()
    
def sigmoid(x):
    return 1/(1+np.exp(-x))


def valve(v,v1,v2):
    if v<v1:
        return 0.0
    elif v<v2:
        return 5e-8
    else:
        return 1e-7
    